package test_controller;


import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import controller.DAOException;
import controller.SugarDAO;
import model.Sugar;

public class SugarDAOTest {
	
	private static SugarDAO mockSugarDAO;
	private static Sugar sugar1;
	private static Sugar sugar2;
	EntityManager em = Mockito.mock(EntityManager.class);
	
	@Before
	public void setUp() throws DAOException {
		// set SugarDAO mock object
		mockSugarDAO = mock(SugarDAO.class);
		
		//create Sugar objects
		sugar1 = new Sugar(100);
		sugar2 = new Sugar(20);
		
		when(mockSugarDAO.getAll(em)).thenReturn(Arrays.asList(sugar1, sugar2));
		
	}

	@Test
    public void getAllTest() throws DAOException {
        List<Sugar> allSugars = mockSugarDAO.getAll(em);
 
        assertNotNull(allSugars);
        assertEquals(2, allSugars.size());
    }
}
