package test_controller;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;

import controller.CustomerDAO;
import model.Customer;

public class CustomerDAOTest {

	CustomerDAO customerDAO;// = Mockito.mock(CustomerDAO.class);
	Customer customer = Mockito.mock(Customer.class);
	
	@InjectMocks
	EntityManager em;// = Mockito.mock(EntityManager.class);

	@Before
	public void setUp()
	{
		customerDAO = new CustomerDAO();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("machinecafe");
		em = emf.createEntityManager();
	}
	
	/*
	@Test
	public void createTest()
	{
		customerDAO.create(em, "test", "test", "test", "test", "test");
		assertNotNull(customerDAO.getCustomerByUsername(em, "test"));
	}*/
	
	@Test
	public void verifyConnectionTest()
	{
		//Username et mdp existant
		assertEquals(customerDAO.verifyConnection(em, "yveneas", "password").getName(), "Perritaz");
		
		//Username / mdp inexistant
		//assertNull(customerDAO.verifyConnection(em, Mockito.anyString(), Mockito.anyString()));
		
		//username vide et mdp inexistant
		//assertNull(customerDAO.verifyConnection(em, "", Mockito.anyString()));
		
		//Username inexistant et mdp vide
		//assertNull(customerDAO.verifyConnection(em, Mockito.anyString(), ""));
		
		//Username / mdp vide
		assertNull(customerDAO.verifyConnection(em, "", ""));
	}
	
	@Test
	public void getCustomerByUsername()
	{
		assertNotNull(customerDAO.getCustomerByUsername(em, "yveneas"));
		assertNull(customerDAO.getCustomerByUsername(em, ""));
		assertNull(customerDAO.getCustomerByUsername(em, null));
		
	}

}
