package test_controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import controller.DAOException;
import controller.DrinkDAO;
import model.Drink;


public class DrinkDAOTest {

	private static DrinkDAO mockDrinkDAO;
	private static Drink drink1;
	private static Drink drink2;
	EntityManager em = Mockito.mock(EntityManager.class);
	
	@Before
	public void setUp() throws DAOException
	{
		mockDrinkDAO = mock(DrinkDAO.class);
		
		//create Drink objects
		drink1 = new Drink("Coca", 15.02f, 100);
		drink2 = new Drink("fanta", 10.02f, 200);
		
		when(mockDrinkDAO.getAll(em)).thenReturn(Arrays.asList(drink1, drink2));
	}
	
	@Test
    public void getAllTest() throws DAOException {
        List<Drink> allDrink = mockDrinkDAO.getAll(em);
 
        assertNotNull(allDrink);
        assertEquals(2, allDrink.size());
    }
}
