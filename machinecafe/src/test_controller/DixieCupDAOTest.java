package test_controller;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import controller.DixieCupDAO;
import model.DixieCup;

public class DixieCupDAOTest {
	
	DixieCupDAO dixieCupDAO;// = Mockito.mock(DixieCupDAO.class);
	DixieCup dixieCup1; //= Mockito.mock(DixieCup.class);
	DixieCup dixieCup2;
	EntityManager em;
	
	@Before
	public void setUp()
	{
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("machinecafe");
		em = emf.createEntityManager();
		dixieCupDAO = new DixieCupDAO();
		
		dixieCup1 = new DixieCup(10, 500);
		dixieCup2 = new DixieCup(5, 500);
		when(dixieCupDAO.getAll(em)).thenReturn(Arrays.asList(dixieCup1, dixieCup2));
		
	}
	
	@Test
	public void getMinVolumCupTest()
	{
		
		assertEquals(5, dixieCupDAO.getMinVolumCup(em));
		assertEquals(-1, dixieCupDAO.getMinVolumCup(em));
	
	}
	/*
	@Test
	public void getFillableDixieCupTest()
	{
		assertNull(mockDixieCupDAO.getFillableDixieCup(em, Mockito.anyInt()));
	}*/
	
	@Test
	public void getAllTest()
	{
		List allDixieCup = dixieCupDAO.getAll(em);
		assertEquals(allDixieCup.size(), 2);
	}
	

}
