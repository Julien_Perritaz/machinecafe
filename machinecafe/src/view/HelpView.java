package view;

public class HelpView
{
	public void displayHelp()
	{
		System.out.println("\n\n\n----------------- HELP -----------------\n");
		System.out.println("- help 	: Affiche cette page d'aide.");
		System.out.println("- commander : Permet de passer une commande.");
		System.out.println("- deconnexion : Permet de se déconnecter");
		System.out.println("\n----------------------------------------\n\n");
	}
}
