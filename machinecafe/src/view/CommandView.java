package view;

import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;

import controller.CommandDAO;
import controller.DAOException;
import controller.DixieCupDAO;
import controller.DrinkDAO;
import model.Customer;
import model.DixieCup;
import model.Drink;

public class CommandView
{
	private DrinkDAO drinkDAO;
	private DixieCupDAO dixieCupDAO;
	private CommandDAO commandDAO;

	public CommandView()
	{
		this.drinkDAO = new DrinkDAO();
		this.dixieCupDAO = new DixieCupDAO();
		this.commandDAO = new CommandDAO();
	}

	public void commandView(EntityManager em, Customer customer) throws DAOException
	{
		List<Drink> availableDrinkList = drinkDAO.getAllRemainingDrink(em);
		if(availableDrinkList != null)
		{
			System.out.println("Voici nos boissons disponible :\n");
			int index = 1;
			for(Drink drink : availableDrinkList)
			{
				System.out.println(index + " - " + drink.getBrand());
				index++;
			}
			int answer = 0;
			do
			{
				try
				{
					System.out.println("Entrez le numéro de la boisson qui vous ferait plaisir\n");
					Scanner scanner = new Scanner(System.in);
					answer = Integer.parseInt(scanner.next());
				}catch(NumberFormatException e)
				{
					System.out.println("Veuillez rentrer uniquement un nombre ou numéro.");
				}
				if(answer > index || answer <= 0)
				{
					System.out.println("Entrez un chiffre/nombre entre 1 et " + index + " s'il vous plaît.");
				}

			}while(answer > index || answer <= 0);

			index = 1;
			answer = 0;
			Drink drink = availableDrinkList.get(answer);

			List<DixieCup> availableDixieCup = dixieCupDAO.getFillableDixieCup(em, drink.getRemainingDrinkQty());

			if(availableDixieCup != null)
			{
				System.out.println("Voici les formats disponibles :\n");
				for(DixieCup dixieCup : availableDixieCup)
				{
					System.out.println(index + " - " + dixieCup.getVolume() + "cl");
					index++;
				}

				do
				{
					try
					{
						System.out.println("Entrez le numéro du volume souhaité\n");
						Scanner scanner = new Scanner(System.in);
						answer = Integer.parseInt(scanner.next());
					}catch(NumberFormatException e)
					{
						System.out.println("Veuillez rentrer uniquement un nombre ou numéro.");
					}
					if(answer > index || answer <= 0)
					{
						System.out.println("Entrez un chiffre/nombre entre 1 et " + index + " s'il vous plaît.");
					}

				}while(answer > index || answer <= 0);

				DixieCup dixieCup = availableDixieCup.get(index);

				answer = -1;
				do
				{

					System.out.println("Pour finir combien de sucre souhaitez-vous ? Entre 0 et 5.\n");
					Scanner scanner = new Scanner(System.in);
					answer = Integer.parseInt(scanner.next());
					if(answer < 0 || answer > 5)
					{
						System.out.println("Vous ne pouvez avoir qu'entre 0 et 5 sucres.");
					}
				}while(answer < 0 || answer > 5);

				int nbSugar = answer;
				System.out.println("Votre commande est en cours de préparation");

				commandDAO.create(em, nbSugar, drink, dixieCup, customer);
			}
			else
			{
				System.out.println("Il ne reste plus de gobelet pouvant contenir le voulme de boisson désiré.");
			}
		}
		else
		{
			System.out.println("Nous sommes désolé mais il n'y a plus de boisson disponible.\n");
		}

	}

	/**
	 * @return the drinkDAO
	 */
	public DrinkDAO getDrinkDAO() {
		return drinkDAO;
	}

	/**
	 * @param drinkDAO the drinkDAO to set
	 */
	public void setDrinkDAO(DrinkDAO drinkDAO) {
		this.drinkDAO = drinkDAO;
	}

	/**
	 * @return the drinkDAO
	 */
	public DixieCupDAO getDixieCupDAO() {
		return dixieCupDAO;
	}

	/**
	 * @param drinkDAO the drinkDAO to set
	 */
	public void setDixieCupDAO(DixieCupDAO dixieCupDAO) {
		this.dixieCupDAO = dixieCupDAO;
	}

	/**
	 * @return the commandDAO
	 */
	public CommandDAO getCommandDAO() {
		return commandDAO;
	}

	/**
	 * @param commandDAO the commandDAO to set
	 */
	public void setCommandDAO(CommandDAO commandDAO) {
		this.commandDAO = commandDAO;
	}
}
