package view;

import java.util.Scanner;

import javax.persistence.EntityManager;

import controller.CustomerDAO;
import controller.DAOException;
import model.Customer;

public class AccountView
{
	private CustomerDAO customerDAO;

	public AccountView()
	{
		customerDAO = new CustomerDAO();
	}

	public Customer createAccount(EntityManager em) throws DAOException
	{

		Scanner scanner = new Scanner(System.in);
		System.out.println("Veuillez saisir votre nom \n");
		scanner = new Scanner(System.in);

		String name = "";
		String firstName = "";
		String username = "";
		String password = "";

		name = scanner.next();

		System.out.println("Veuillez saisir votre prénom \n");
		scanner = new Scanner(System.in);
		name = scanner.next();

		do
		{
			System.out.println("Veuillez saisir un nom d'utilisateur \n");
			scanner = new Scanner(System.in);
			username = scanner.next();
			if(customerDAO.getCustomerByUsername(em, username) != null)
			{
				System.out.println("Ce nom d'utilisateur existe déjà.");
				username = "";
			}

		}while(username.equals(""));

		String passwordTemp = "";
		do
		{
			System.out.println("Veuillez saisir votre mot de passe. \n");
			scanner = new Scanner(System.in);
			passwordTemp = scanner.next();

			System.out.println("Veuillez saisir de nouveau votre mot de passe.\n");
			scanner = new Scanner(System.in);
			password = scanner.next();
			
			if(!passwordTemp.equals(password))
			{
				System.out.println("Vous les deux sasies ne correspondent pas.\n");
			}

		}while(!passwordTemp.equals(password));

		return customerDAO.create(em, name, firstName, username, "user", password);
	}

	public Customer connection(EntityManager em)
	{
		Scanner scanner = new Scanner(System.in);
		String username = "";
		String password = "";
		Customer customer = new Customer();
		do
		{
			System.out.println("Veuillez rentrer votre nom d'utilisateur \n");
			scanner = new Scanner(System.in);
			username = scanner.next();
			customer = customerDAO.getCustomerByUsername(em, username);
			if(customer == null)
			{
				System.out.println("Ce nom d'utilisateur n'existe pas.");
				username = "";
			}

		}while(username.equals(""));

		System.out.println("Veuillez saisir votre mot de passe.\n");
		scanner = new Scanner(System.in);
		password = scanner.next();
		if(!password.equals(customer.getPassword()))
		{
			System.out.println("Mauvais mot de passe.\n");
			return null;

		}
		return customer;
	}
}
