package view;

import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import controller.CustomerDAO;
import controller.DAOException;
import model.Customer;

public class Application
{
	public static void main(String args[]) throws DAOException
	{
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("machinecafe");
		EntityManager em = emf.createEntityManager();
		
		CustomerDAO customerDAO = new CustomerDAO();

		AccountView accountView = new AccountView();
		HelpView helpView = new HelpView();
		CommandView commandView = new CommandView();

		Customer customer = new Customer();

		System.out.println("\nAvez-vous un compte ? Oui ou non\n");
		Scanner scanner = new Scanner(System.in);
		String answer = scanner.next();

		do
		{
			if(answer.equalsIgnoreCase("non"))
			{
				customer = accountView.createAccount(em);
			}
			else if(answer.equalsIgnoreCase("oui"))
			{
				customer = accountView.connection(em);
			}
			else
			{
				System.out.println("Il n'est possible de répondre que par oui ou par non.");
			}
		}while(customer  == null);

		answer = "help";
		while(!answer.equalsIgnoreCase("deconnexion"))
		{
			try
			{
				String[] words = answer.split(" ");

				//Display help view
				if(words[0].equalsIgnoreCase("help"))
				{
					helpView.displayHelp();
				}

				//Display command view
				else if(words[0].equalsIgnoreCase("commander"))
				{
					commandView.commandView(em, customer);
				}
				else if(!words[0].equalsIgnoreCase("deconnexion"))
				{
					System.out.println("\nCommande incorrecte\n");
					helpView.displayHelp();
				}
			}
			catch(NumberFormatException e)
			{

			}
			System.out.printf("\n>> ");
			answer = scanner.next();
		
		}
	
	}
}
