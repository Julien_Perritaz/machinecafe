package model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Command
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCommand;

	private int nbSugar;

	@ManyToOne(cascade = CascadeType.ALL)
	private Drink drink;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private DixieCup dixieCup;

	@ManyToOne(cascade = CascadeType.ALL)
	private Customer customer;

	@ManyToOne(cascade = CascadeType.ALL)
	private Sugar sugar;
	
	/**
	 * builds Command
	 * @param idCommand
	 * @param nbSugar
	 * @param drink
	 * @param dixieCup
	 * @param customer
	 * @param sugar
	 */
	public Command(Customer customer, Drink drink, int nbSugar, DixieCup dixieCup)
	{
		this.customer = customer;
		this.drink = drink;
		this.nbSugar = nbSugar;
		this.dixieCup = dixieCup;
	}
	
	public Command()
	{
		
	}

	/**
	 * gets idCommand
	 * @return idCommand
	 */
	
	public int getIdCommand() {
		return idCommand;
	}
	
	/**
	 * sets idCommand
	 * @param idCommand
	 */
	public void setIdCommand(int idCommand) {
		this.idCommand = idCommand;
	}
	
	/**
	 * gets dixieCup
	 * @return dixieCup
	 */
	public DixieCup getDixieCup() {
		return dixieCup;
	}

	/**
	 * sets dixieCup
	 * @param dixieCup
	 */
	public void setDixieCup(DixieCup dixieCup) {
		this.dixieCup = dixieCup;
	}
	
	/**
	 * gets Customer
	 * @return customer
	 */
	public Customer getCustomer() {
		return customer;
	}
	
	/**
	 * sets the customer
	 * @param customer
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the nbSugar
	 */
	public int getNbSugar() {
		return nbSugar;
	}

	/**
	 * @param nbSugar the nbSugar to set
	 */
	public void setNbSugar(int nbSugar) {
		if(nbSugar < 0)
		{
			//Throw an exception
		}
		this.nbSugar = nbSugar;
	}

	/**
	 * @return the drink
	 */
	public Drink getDrink() {
		return drink;
	}

	/**
	 * @param drink the drink to set
	 */
	public void setDrink(Drink drink) {
		this.drink = drink;
	}
}
