package model;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
/**
 * Customer class
 *
 */
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int idCustomer;
	
	String name;
	String firstname;
	String username;
	String status;
	String password;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer")
    private List<Command> commands;
	/**
	 * Class' builder
	 * @param idCustomer
	 * @param name
	 * @param firstname
	 * @param username
	 * @param status
	 * @param password
	 */
	public Customer(String name, String firstname, String username, String status, String password) {
		this.name = name;
		this.firstname = firstname;
		this.username = username;
		this.status = status;
		this.password = password;
		this.commands = new ArrayList<>();
	}
	public Customer() {
		this.commands = new ArrayList<>();
	}

	/**
	 * idCustomer's getter
	 * @return idCustomer
	 */
	public int getIdCustomer() {
		return idCustomer;
	}

	/**
	 * idCustomer's setter
	 * @param idCustomer
	 */
	public void setIdCustomer(int idCustomer) {
		this.idCustomer = idCustomer;
	}

	/**
	 * name's getter
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * name's setter
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * firstname's getter
	 * @return firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * firstname's setter
	 * @param firstname
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * username's getter
	 * @return username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * username's setter
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * status' getter
	 * @return status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * status' setter
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * password's getter
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * password's setter
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 *
	 * @return
	 */
	public List<Command> getCommands() {
		return commands;
	}

	/**
	 *
	 * @param commands
	 */
	public void setCommands(List<Command> commands) {
		this.commands = commands;
	}


	public void addCommand(Command c) {
		this.commands.add(c);
	}

	@Override
	public String toString() {
		return "Customer [name=" + name + ", firstname=" + firstname + ", username=" + username + ", status="+status+", password="+password+"]";
	}
}
