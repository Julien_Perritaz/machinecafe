package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class DixieCup {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idDixieCup;
	private int volume;
	private int remainingCupQty;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sugar")
    private List<Command> commands;

    public DixieCup() {
        commands = new ArrayList<>();
    }

    public DixieCup(int volume, int remainingCupQty) {
        commands = new ArrayList<>();
        this.volume = volume;
        this.remainingCupQty = remainingCupQty;
    }

	public int getIdDixieCup() {
		return idDixieCup;
	}

	public void setIdDixieCup(int idDixieCup) {
		this.idDixieCup = idDixieCup;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public int getRemainingCupQty() {
		return remainingCupQty;
	}

	public void setRemainingCupQty(int remainingCupQty) {
		this.remainingCupQty = remainingCupQty;
	}

	public List<Command> getCommands() {
		return commands;
	}

	public void setCommands(List<Command> commands) {
		this.commands = commands;
	}

	public void addCommand(Command c) {
		this.commands.add(c);
	}
}
