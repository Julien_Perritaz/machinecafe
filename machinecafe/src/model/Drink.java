package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Drink {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idDrink;
	private String brand;
	private float price;
	private int remainingDrinkQty;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "drink")
    private List<Command> commands;
    
	/**
	 * Class' builder
	 * @param brand Brand of the drink
	 * @param price Price of the drink
	 * @param RemainingDrinkQty The remaining quantity oh thid drink
	 */
	public Drink(String brand, float price, int remainingDrinkQty) {
		this.brand = brand;
		this.price = price;
		this.remainingDrinkQty = remainingDrinkQty;
		this.commands = new ArrayList<>();
	}
	public Drink() {
		this.commands = new ArrayList<>();
	}
	/**
	 * IdDrink's getter
	 * @return idDrink
	 */
	public int getIdDrink() {
		return idDrink;
	}
	/**
	 * IdDrink's setter
	 * @param idDrink
	 */
	public void setIdDrink(int idDrink){
		this.idDrink = idDrink;
	}

	/**
	 * Brand's getter
	 * @return brand
	 */
	public String getBrand() {
		return brand;
	}
	/**
	 * Brand's setter
	 * @param brand
	 */
	public void setBrand(String brand){
		this.brand = brand;
	}

	/**
	 * price's getter
	 * @return price
	 */
	public float getPrice() {
		return price;
	}
	/**
	 * price's setter
	 * @param price
	 */
	public void setPrice(float price){
		this.price = price;
	}


	/**
	 * RemainingDrinkQty's getter
	 * @return RemainingDrinkQty
	 */
	public int getRemainingDrinkQty() {
		return remainingDrinkQty;
	}
	/**
	 * RemainingDrinkQty's setter
	 * @param RemainingDrinkQty
	 */
	public void setRemainingDrinkQty(int remainingDrinkQty){
		this.remainingDrinkQty = remainingDrinkQty;
	}

	public List<Command> getCommands() {
		return commands;
	}
	public void setCommands(List<Command> commands) {
		this.commands = commands;
	}
	public void addCommand(Command command) {
		this.commands.add(command);
	}
}
