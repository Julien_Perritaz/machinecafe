package model;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Sugar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idSugar;
    private double remainingSugarQty;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sugar")
    private List<Command> commands;

    public List<Command> getCommands() {
		return commands;
	}

	public void setCommands(List<Command> commands) {
		this.commands = commands;
	}

	public Sugar() {
        commands = new ArrayList<>();
    }

    public Sugar(double remainingSugarQty) {

        this.setRemainingSugarQty(remainingSugarQty);
        commands = new ArrayList<>();
    }

    public void addCommand(Command c){
        commands.add(c);
    }

	/**
	 * @return the remainingSugarQty
	 */
	public double getRemainingSugarQty() {
		return remainingSugarQty;
	}

	/**
	 * @param remainingSugarQty the remainingSugarQty to set
	 */
	public void setRemainingSugarQty(double remainingSugarQty) {
		this.remainingSugarQty = remainingSugarQty;
	}

	/**
	 * @return the idSugar
	 */
	public int getIdSugar() {
		return idSugar;
	}

	/**
	 * @param idSugar the idSugar to set
	 */
	public void setIdSugar(int idSugar) {
		this.idSugar = idSugar;
	}

}