package controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Command;
import model.Drink;

public class DrinkDAO {
	
	
	/**
	 * Get every Drink in database
	 * 
	 * @param em
	 * 
	 * @return listDrink
	 * 
	 * @throws DAOException
	 */
	public List<Drink> getAll(EntityManager em)throws DAOException {
		try {
			Query query = em.createQuery("from Drink");

			@SuppressWarnings("unchecked")
			List<Drink> listDrink = query.getResultList();

			return listDrink;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC getAll Drink: " + sqle);
			System.exit(1);
		}
		return null;
	}
	
	
	/**
	 * Create a new Drink
	 * 
	 * @param em                The entity Manager
	 * @param idDrink           Id of the drink
	 * @param brand             Brand of the drink
	 * @param price             Price of the drink
	 * @param RemainingDrinkQty The remaining quantity oh thid drink
	 *
	 * @return The created drink
	 * @throws DAOException
	 */
	public static Drink create(EntityManager em, String brand, float price, int RemainingDrinkQty) throws DAOException {
		try {
			Drink drink = new Drink(brand, price, RemainingDrinkQty);

			em.getTransaction().begin();
			em.persist(drink);
			em.getTransaction().commit();
			return drink;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Drink create : " + sqle);
			System.exit(1);
		}
		return null;
	}

	/**
	 * Modify a Drink
	 * 
	 * @param em                    The entity Manager
	 * @param new_idDrink           New Id of the drink
	 * @param new_brand             New Brand of the drink
	 * @param new_price             New Price of the drink
	 * @param new_RemainingDrinkQty The New remaining quantity oh thid drink
	 * 
	 * @return the modified Drink
	 * 
	 * @throws DAOException
	 */
	public static Drink modify(EntityManager em, String newBrand, float newPrice, int newRemainingDrinkQty)
			throws DAOException {
		try {
			Drink drink = new Drink(newBrand, newPrice, newRemainingDrinkQty);
			em.getTransaction().begin();
			em.persist(drink);
			em.getTransaction().commit();

			return drink;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Drink modify : " + sqle);
			System.exit(1);
		}
		return null;
	}

	/**
	 * Delete a Drink
	 * 
	 * @param em    The entity Manager
	 * @param drink The drink to delete
	 * 
	 * @throws DAOException
	 */
	public static void delete(EntityManager em, Drink drink) throws DAOException {
		try {
			em.getTransaction().begin();
			for (Command command : drink.getCommands()) {
				em.remove(command);
			}
			em.remove(drink);
			em.getTransaction().commit();
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Drink delete : " + sqle);
			System.exit(1);
		}
	}

	/**
	 * Get all remaining drink
	 * 
	 * @return List of all remaining drink
	 */
	public List<Drink> getAllRemainingDrink(EntityManager em) {
		DixieCupDAO dixieCupDAO = new DixieCupDAO();

		// Getting minimum volume
		int minVolume = dixieCupDAO.getMinVolumCup(em);
		if (minVolume > -1) {
			Query query = em.createQuery("from Drink WHERE remainingDrinkQty > :minVolume");
			query.setParameter("minVolume", minVolume);
			@SuppressWarnings("unchecked")
			List<Drink> drinkList = query.getResultList();

			return drinkList;
		}

		return null;

	}
}
