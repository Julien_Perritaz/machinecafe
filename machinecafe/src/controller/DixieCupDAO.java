package controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.QueryException;

import model.Command;
import model.DixieCup;

public class DixieCupDAO {

	
	public static DixieCup create(EntityManager em, int idDixieCup, int volume, int remainingCupQty)
			throws DAOException {
		try {
			DixieCup dixie_cup = new DixieCup();
			dixie_cup.setIdDixieCup(idDixieCup);
			dixie_cup.setVolume(volume);
			dixie_cup.setRemainingCupQty(remainingCupQty);

			em.getTransaction().begin();
			em.persist(dixie_cup);
			em.getTransaction().commit();
			return dixie_cup;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC DixieCup create : " + sqle);
			System.exit(1);

		}
		return null;
	}
	
	public List<DixieCup> getAll(EntityManager em)
	{
		try
		{
			Query query = em.createQuery("from DixieCup");
			List<DixieCup> dixieCupList = query.getResultList();

			return dixieCupList;
		}catch(NoResultException | QueryException e)
		{
			return null;
		}
	}
	
	public static DixieCup modify(EntityManager em, int new_idDixieCup, int new_volume, int new_remainingCupQty)
			throws DAOException {
		try {
			DixieCup dixie_cup = new DixieCup();
			dixie_cup.setIdDixieCup(new_idDixieCup);
			dixie_cup.setVolume(new_volume);
			dixie_cup.setRemainingCupQty(new_remainingCupQty);

			em.getTransaction().begin();
			em.persist(dixie_cup);
			em.getTransaction().commit();
			return dixie_cup;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC DixieCup modify : " + sqle);
			System.exit(1);
		}
		return null;
	}

	public static void delete(EntityManager em, DixieCup dixie_cup) throws DAOException {
		try {
			em.getTransaction().begin();
			for (Command command : dixie_cup.getCommands()) {
				em.remove(command);
			}
			em.remove(dixie_cup);
			em.getTransaction().commit();
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC DixieCup delete : " + sqle);
			System.exit(1);
		}
	}

	public int getMinVolumCup(EntityManager em) {
		try {
			Query query = em.createQuery("SELECT MIN(volume) from DixieCup");
			List<DixieCup> dixieCupList = query.getResultList();

			// If it have more than one result they have the same volume.
			return dixieCupList.get(0).getVolume();
		} catch (NoResultException | QueryException e) {
			return -1;
		}
		
	}

	public List<DixieCup> getFillableDixieCup(EntityManager em, int volume) {
		try {
			Query query = em.createQuery("from DixieCup WHERE volume <= :volume AND remainingCupQty >= 1");
			query.setParameter("volume", volume);
			List<DixieCup> dixieCupList = query.getResultList();
			if (dixieCupList.isEmpty()) {
				return null;
			}
			// If it have more than one result they have the same volume.
			return dixieCupList;
		} catch (NoResultException e) {
			return null;
		}
	}
}
