package controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Command;
import model.Customer;
import model.DixieCup;
import model.Drink;
import model.Sugar;

public class CommandDAO {
	
	/**
	 * Get every Command in database
	 * 
	 * @param em
	 * 
	 * @return listCommand
	 * 
	 * @throws DAOException
	 */
	public List<Command> getAll(EntityManager em)throws DAOException {
		try {
			Query query = em.createQuery("from Command");

			@SuppressWarnings("unchecked")
			List<Command> listCommand = query.getResultList();

			return listCommand;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC getAll Command: " + sqle);
			System.exit(1);
		}
		return null;
	}
	
	
	/**
	 * Create a new Command
	 * 
	 * @param em
	 * @param nbSugar
	 * @param drink
	 * @param dixie_cup
	 * @param customer
	 * 
	 * @return the created command
	 * 
	 * @throws DAOException
	 */
	public Command create(EntityManager em, int nbSugar, Drink drink, DixieCup dixie_cup, Customer customer)
			throws DAOException {
		try {
			Command command = new Command();
			// command.setIdCommand(idCommand);
			command.setNbSugar(nbSugar);
			command.setDrink(drink);
			drink.addCommand(command);
			command.setDixieCup(dixie_cup);
			dixie_cup.addCommand(command);
			command.setCustomer(customer);
			customer.addCommand(command);
			// command.setSugar(sugar);
			// sugar.addCommand(command);

			em.getTransaction().begin();
			em.persist(customer);
			em.persist(dixie_cup);
			em.persist(drink);
			// em.persist(sugar);
			em.persist(command);
			em.getTransaction().commit();
			return command;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Command create : " + sqle);
			System.exit(1);
		}
		return null;
	}

	/**
	 * Modify a Command
	 * 
	 * @param em
	 * @param new_idCommand
	 * @param new_nbSugar
	 * @param new_drink
	 * @param new_dixie_cup
	 * @param new_customer
	 * @param new_sugar
	 * 
	 * @return the modified command
	 * 
	 * @throws DAOException
	 */
	public static Command modify(EntityManager em, int new_idCommand, int new_nbSugar, Drink new_drink,
			DixieCup new_dixie_cup, Customer new_customer, Sugar new_sugar) throws DAOException {
		try {
			Command command = new Command();
			command.setIdCommand(new_idCommand);
			command.setNbSugar(new_nbSugar);
			command.setDrink(new_drink);
			command.setDixieCup(new_dixie_cup);
			command.setCustomer(new_customer);
			//command.setSugar(new_sugar);

			em.getTransaction().begin();
			em.persist(command);
			em.getTransaction().commit();
			return command;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Command modify : " + sqle);
			System.exit(1);
		}
		return null;
	}

	/**
	 * Delete a Command
	 * 
	 * @param em
	 * @param command
	 * 
	 * @throws DAOException
	 */
	public void delete(EntityManager em, Command command) throws DAOException {
		try {
			em.getTransaction().begin();
			em.remove(command);
			em.getTransaction().commit();
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Command delete : " + sqle);
			System.exit(1);
		}
	}

}
