package controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Command;
import model.Sugar;

public class SugarDAO {
	
	/**
	 * Get every Sugar in database
	 * 
	 * @param em
	 * 
	 * @return listSugar
	 * 
	 * @throws DAOException
	 */
	public List<Sugar> getAll(EntityManager em)throws DAOException {
		try {
			Query query = em.createQuery("from Sugar");


			@SuppressWarnings("unchecked")
			List<Sugar> listSugar = query.getResultList();

			return listSugar;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC getAll Sugar: " + sqle);
			System.exit(1);
		}
		return null;
	}
	
	/**
	 * Create a new Sugar
	 * 
	 * @param em
	 * @param idSugar
	 * @param remainingSugarQty
	 * 
	 * @return the created sugar
	 * 
	 * @throws DAOException
	 */
	public static Sugar create(EntityManager em, int idSugar, double remainingSugarQty) throws DAOException {
		try {
			Sugar sugar = new Sugar();
			sugar.setIdSugar(idSugar);
			sugar.setRemainingSugarQty(remainingSugarQty);

			em.getTransaction().begin();
			em.persist(sugar);
			em.getTransaction().commit();
			return sugar;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Sugar create : " + sqle);
			System.exit(1);
		}
		return null;
	}

	/**
	 * Modify a Sugar
	 * 
	 * @param em
	 * @param new_idSugar
	 * @param new_remainingSugarQty
	 * 
	 * @return the modified sugar 
	 * 
	 * @throws DAOException
	 */
	public static Sugar modify(EntityManager em, int new_idSugar, double new_remainingSugarQty) throws DAOException {
		try {
			Sugar sugar = new Sugar();
			sugar.setIdSugar(new_idSugar);
			sugar.setRemainingSugarQty(new_remainingSugarQty);

			em.getTransaction().begin();
			em.persist(sugar);
			em.getTransaction().commit();
			return sugar;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Sugar modify : " + sqle);
			System.exit(1);
		}
		return null;
	}

	
	/**
	 * Delete a Sugar
	 * 
	 * @param em
	 * @param sugar
	 * 
	 * @throws DAOException
	 */
	public static void delete(EntityManager em, Sugar sugar) throws DAOException {
		try {
			em.getTransaction().begin();
			for (Command command : sugar.getCommands()) {
				em.remove(command);
			}
			em.remove(sugar);
			em.getTransaction().commit();
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Sugar delete : " + sqle);
			System.exit(1);
		}

	}

}
