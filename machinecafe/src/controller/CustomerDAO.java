package controller;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.QueryException;

import model.Command;
import model.Customer;

public class CustomerDAO {
	
	/**
	 * Get every Customer in database
	 * @param em
	 * @return listCustomer
	 * @throws DAOException
	 */
	public List<Customer> getAll(EntityManager em)throws DAOException {
		try {
			Query query = em.createQuery("from Sugar");

			@SuppressWarnings("unchecked")
			List<Customer> listCustomer = query.getResultList();

			return listCustomer;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC getAll Customer: " + sqle);
			System.exit(1);

		}
		return null;
	}
	
	/**
	 * Create a Customer
	 * 
	 * @param em
	 * @param name
	 * @param firstname
	 * @param username
	 * @param status
	 * @param password
	 * 
	 * @return the created customer
	 * 
	 * @throws DAOException
	 */
	public static Customer create(EntityManager em, String name, String firstname, String username, String status,
			String password) throws DAOException {
		try {
			Customer customer = new Customer();

			customer.setName(name);
			customer.setFirstname(firstname);
			customer.setUsername(username);
			customer.setStatus(status);
			customer.setPassword(password);
			em.getTransaction().begin();
			em.persist(customer);
			em.getTransaction().commit();
			return customer;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Customer create : " + sqle);
			System.exit(1);
		}
		return null;
	}

	/**
	 * Modify a Customer
	 * 
	 * @param em
	 * @param new_name
	 * @param new_firstname
	 * @param new_username
	 * @param new_status
	 * @param new_password
	 * 
	 * @return the modified customer
	 * 
	 * @throws DAOException
	 */
	public static Customer modify(EntityManager em, String new_name, String new_firstname, String new_username,
			String new_status, String new_password) throws DAOException {
		try {
			Customer customer = new Customer();

			customer.setName(new_name);
			customer.setFirstname(new_firstname);
			customer.setUsername(new_username);
			customer.setStatus(new_status);
			customer.setPassword(new_password);
			em.getTransaction().begin();
			em.persist(customer);
			em.getTransaction().commit();

			return customer;
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Customer modify : " + sqle);
			System.exit(1);
		}
		return null;
	}

	/**
	 * Delete a Customer
	 * 
	 * @param em
	 * @param customer
	 * 
	 * @throws DAOException
	 */
	public static void delete(EntityManager em, Customer customer) throws DAOException {
		try {
			em.getTransaction().begin();
			for (Command command : customer.getCommands()) {
				em.remove(command);
			}
			em.remove(customer);
			em.getTransaction().commit();
		} catch (Exception sqle) {
			System.out.println("Erreur JDBC Customer delete : " + sqle);
			System.exit(1);
		}
	}

	/**
	 * Verify if the user exists in database
	 * @param em
	 * @param username
	 * @param password
	 * @return the customer
	 */
	public Customer verifyConnection(EntityManager em, String username, String password) {
		Query query = em.createQuery("from Customer WHERE username= :username AND password= :password");
		query.setParameter("username", username);
		query.setParameter("password", password);
		Customer customer = new Customer();
		try {
			customer = (Customer) query.getSingleResult();
		} catch (QueryException | NoResultException e) {
			return null;
		}
		return customer;
	}

	/**
	 * Get a customer by his username
	 * 
	 * @param em
	 * @param username
	 * 
	 * @return return the customer with the username
	 */
	public Customer getCustomerByUsername(EntityManager em, String username) {
		Query query = em.createQuery("from Customer WHERE username= :username");
		query.setParameter("username", username);
		Customer customer = new Customer();
		try {
			customer = (Customer) query.getSingleResult();
		} catch (QueryException | NoResultException e) {
			return null;
		}
		return customer;
	}
}
