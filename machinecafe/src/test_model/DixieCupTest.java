package test_model;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import model.Command;
import model.DixieCup;

public class DixieCupTest {

	DixieCup dixieCup;
	
	@Before
	public void setUp()
	{
		dixieCup = new DixieCup();
		dixieCup = new DixieCup(33, 500);
	}
	
	@Test
	public void getVolumeTest()
	{
		assertEquals(dixieCup.getVolume(), 33);
	}
	
	@Test
	public void setVolume()
	{
		dixieCup.setVolume(100);
		assertEquals(dixieCup.getVolume(), 100);
	}
	
	@Test
	public void getRemainingCupQtyTest()
	{
		assertEquals(dixieCup.getRemainingCupQty(), 500);
	}
	
	@Test
	public void setRemainingCupQtyTest()
	{
		dixieCup.setRemainingCupQty(1000);
		assertEquals(dixieCup.getRemainingCupQty(), 1000);
	}
	
	@Test
	public void getIdDixieCupTest()
	{
		assertEquals(dixieCup.getIdDixieCup(), 0);
	}
	
	@Test
	public void setIdDixieCupTest()
	{
		dixieCup.setIdDixieCup(1);
		assertEquals(dixieCup.getIdDixieCup(), 1);
	}
	
	@Test
	public void getCommandsTest()
	{
		List<Command> list = new ArrayList<Command>();
		assertEquals(dixieCup.getCommands(), list);
	}
	
	@Test
	public void setCommandTest()
	{
		List<Command> list = new ArrayList<Command>();
		Command command = Mockito.mock(Command.class);
		list.add(command);
		dixieCup.setCommands(list);
		assertEquals(dixieCup.getCommands(), list);
	}
	
	@Test
	public void addCommandTest()
	{
		Command command = Mockito.mock(Command.class);
		dixieCup.addCommand(command);
		assertEquals(dixieCup.getCommands().size(), 1);
	}
	

}
