package test_model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import model.Command;
import model.Sugar;

public class SugarTest {

	Sugar sugar;
	
	@Before
	public void setUp()
	{
		sugar = new Sugar();
		sugar = new Sugar(90);
	}
	
	@Test
	public void getIdSugarTest()
	{
		assertEquals(sugar.getIdSugar(), 0);
	}
	
	@Test
	public void setIgSugarTest()
	{
		sugar.setIdSugar(1);
		assertEquals(sugar.getIdSugar(), 1);
	}
	
	@Test
	public void getRemainingSugarQtyTest()
	{
		assertEquals(sugar.getRemainingSugarQty(), 90);
	}
	
	@Test
	public void setRemainingSugarQtyTest()
	{
		sugar.setRemainingSugarQty(190);
		assertEquals(sugar.getRemainingSugarQty(), 190);
	}
	
	@Test
	public void getCommandsTest()
	{
		List<Command> list = new ArrayList<Command>();
		assertEquals(sugar.getCommands(), list);
	}
	
	@Test
	public void setCommandTest()
	{
		List<Command> list = new ArrayList<Command>();
		Command command = new Command();
		list.add(command);
		sugar.setCommands(list);
		assertEquals(sugar.getCommands(), list);
	}
	
	@Test
	public void addCommandTest()
	{
		Command command = new Command();
		sugar.addCommand(command);
		assertEquals(sugar.getCommands().size(), 1);
	}

}
