package test_model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import model.Command;
import model.Drink;

public class DrinkTest {

	Drink drink;
	
	@Before
	public void setUp()
	{
		drink = new Drink();
		drink = new Drink("Nestle", (float) 2.5, 500);
	}
	
	@Test
	public void getBrandTest()
	{
		assertEquals(drink.getBrand(), "Nestle");
	}
	
	@Test
	public void setBrandTest()
	{
		drink.setBrand("Nescafe");
		assertEquals(drink.getBrand(), "Nescafe");
	}
	
	@Test
	public void getPriceTest()
	{
		assertEquals(drink.getPrice(), (float) 2.5);
	}
	
	@Test
	public void setPriceTest()
	{
		drink.setPrice((float) 5.0);
		assertEquals(drink.getPrice(), (float) 5.0);
	}
	
	@Test
	public void getRemainingQtyTest()
	{
		assertEquals(drink.getRemainingDrinkQty(), 500);
	}
	
	@Test
	public void setRemainingQtyTest()
	{
		drink.setRemainingDrinkQty(1000);
		assertEquals(drink.getRemainingDrinkQty(), 1000);
	}
	
	@Test
	public void getIdDrinkTest()
	{
		assertEquals(drink.getIdDrink(), 0);
	}
	
	@Test
	public void setIdDrinkTest()
	{
		drink.setIdDrink(1);
		assertEquals(drink.getIdDrink(), 1);
	}
	
	@Test
	public void getCommandTest()
	{
		List<Command> list = new ArrayList<Command>();
		assertEquals(drink.getCommands(), list);
	}
	
	@Test
	public void setCommandTest()
	{
		List<Command> list = new ArrayList<Command>();
		Command command = Mockito.mock(Command.class);
		list.add(command);
		drink.setCommands(list);
		assertEquals(drink.getCommands(), list);
	}
	
	@Test
	public void addCommandTest()
	{
		Command command = Mockito.mock(Command.class);
		drink.addCommand(command);
		assertEquals(drink.getCommands().size(), 1);
	}
	

}
