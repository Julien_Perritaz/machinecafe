package test_model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.Test;
import org.mockito.Mockito;

import model.Command;
import model.Customer;
import model.DixieCup;
import model.Drink;

public class CommandTest {

	Command command = null;
	
	Customer customer = Mockito.mock(Customer.class);
	Drink drink = Mockito.mock(Drink.class);
	DixieCup dixieCup = Mockito.mock(DixieCup.class);
	
	
	//Sugar sugar = Mockito.mock(Sugar.class);
	
	@Before
	public void setUp()
	{
		command = new Command();
		command = new Command(customer, drink, 0, dixieCup);
	}
	
	@Test
	public void getIdCommandTest()
	{
		assertEquals(command.getIdCommand(), 0);
	}
	
	@Test
	public void setCommandIdTest()
	{
		command.setIdCommand(1);
		assertEquals(command.getIdCommand(), 1);
	}
	
	@Test
	public void getCustomerTest()
	{
		assertEquals(command.getCustomer(), customer);
	}
	
	@Test 
	public void setCustomerTest()
	{
		Customer customerTest = Mockito.mock(Customer.class);
		command.setCustomer(customerTest);
		assertEquals(command.getCustomer(), customerTest);
	}
	
	@Test
	public void getDrinkTest()
	{
		assertEquals(command.getDrink(), drink);
	}
	
	@Test
	public void setDrinkTest()
	{
		Drink drinkTemp = Mockito.mock(Drink.class);
		command.setDrink(drinkTemp);
		assertEquals(command.getDrink(), drinkTemp);
	}
	
	@Test
	public void getDixieCupTest()
	{
		assertEquals(command.getDixieCup(), dixieCup);
	}
	
	@Test
	public void setDixieCupeTest()
	{
		DixieCup dixieCupTest = Mockito.mock(DixieCup.class);
		command.setDixieCup(dixieCupTest);
		assertEquals(command.getDixieCup(), dixieCupTest);
	}
	
	@Test
	public void getNbSugarTest()
	{
		assertEquals(command.getNbSugar(), 0);
	}
	
	@Test
	public void setNbSugarTest()
	{
		command.setNbSugar(1);
		assertEquals(command.getNbSugar(), 1);
	}
}
