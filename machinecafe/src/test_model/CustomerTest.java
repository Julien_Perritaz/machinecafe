package test_model;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import model.Command;
import model.Customer;

public class CustomerTest {

	Customer customer;

	@Before
	public void setUp()
	{
		customer = new Customer();
		customer = new Customer("Boon", "Dany", "Dany Boon", "user", "chtipower");
	}
	
	@Test
	public void getNameTest() {
		assertEquals(customer.getName(), "Boon");
	}
	
	@Test
	public void setNameTest() {
		 
		customer.setName("Boune");
		assertEquals(customer.getName(), "Boune");
	}

	@Test
	public void getFirstnameTest() {
		 
		assertEquals(customer.getFirstname(), "Dany");
	}
	@Test
	public void setFirstnameTest() {
		 
		customer.setFirstname("Daniel");
		assertEquals(customer.getFirstname(), "Daniel");
		
		customer.setFirstname(null);
		assertEquals(customer.getFirstname(), null);
	}
	@Test
	public void getUsernameTest() {
		 
		assertEquals(customer.getUsername(), "Dany Boon");
	}
	@Test
	public void setUsernameTest() {
		 
		customer.setUsername("Daniel Boune");
		assertEquals(customer.getUsername(), "Daniel Boune");
		
		customer.setUsername(null);
		assertEquals(customer.getUsername(), null);
	}
	@Test
	public void getStatusTest() {
		 
		assertEquals(customer.getStatus(), "user");
	}
	@Test
	public void setStatusTest() {
		 
		customer.setStatus("admin");
		assertEquals(customer.getStatus(), "admin");
		
		customer.setStatus(null);
		assertEquals(customer.getStatus(), null);
	}
	
	@Test
	public void getPasswordTest() {
		 
		assertEquals(customer.getPassword(), "chtipower");
	}
	
	@Test
	public void setPasswordTest() {
		 
		customer.setPassword("ch'tipower");
		assertEquals(customer.getPassword(), "ch'tipower");
		
		customer.setPassword(null);
		assertEquals(customer.getPassword(), null);
	}
	
	@Test
	public void toStringTest() {
		String expected_string = "Customer [name=Boon, firstname=Dany, username=Dany Boon, status=user, password=chtipower]";
		 
		assertEquals(expected_string, customer.toString());
	}
	
	@Test
	public void getIdCustomerTest()
	{
		assertEquals(customer.getIdCustomer(), 0);
	}
	
	@Test
	public void setIdCustomerTest()
	{
		customer.setIdCustomer(1);
		assertEquals(customer.getIdCustomer(), 1);
	}
	
	@Test
	public void getCommandsTest()
	{
		List<Command> list = new ArrayList<Command>();
		assertEquals(customer.getCommands(), list);
	}
	
	@Test
	public void setCommandTest()
	{
		Command command = Mockito.mock(Command.class);
		List<Command> list = new ArrayList<Command>();
		list.add(command);
		customer.setCommands(list);
		assertEquals(customer.getCommands(), list);
	}
	
	@Test
	public void addCommandTest()
	{
		Command command = Mockito.mock(Command.class);
		customer.addCommand(command);
		assertEquals(customer.getCommands().size(), 1);
	}


}
