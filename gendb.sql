DROP DATABASE IF EXISTS machinecafe;
CREATE DATABASE machinecafe;
USE machinecafe;
CREATE TABLE Customer
(
idCustomer INT NOT NULL PRIMARY KEY,
name VARCHAR(30),
firstname VARCHAR(30),
username VARCHAR(30) NOT NULL,
status VARCHAR(30) NOT NULL,
password VARCHAR(30) NOT NULL
);
CREATE TABLE Sugar
(
idSugar INT NOT NULL PRIMARY KEY,
remainingSugarQty INT NOT NULL
);
CREATE TABLE Drink
(
idDrink INT NOT NULL PRIMARY KEY,
brand VARCHAR(30) NOT NULL,
price FLOAT NOT NULL, 
remainingDrinkQty INT NOT NULL
);
CREATE TABLE DixieCup
(
idDixieCup INT NOT NULL PRIMARY KEY,
volume INT NOT NULL,
remainingCupQty INT NOT NULL
);
CREATE TABLE Command
(
idCommand INT NOT NULL PRIMARY KEY,
nbSugar INT,
idCustomer INT NOT NULL,
idSugar INT,
idDrink INT NOT NULL,
idDixieCup INT NOT NULL,
FOREIGN KEY (idCustomer) REFERENCES Customer(idCustomer),
FOREIGN KEY (idSugar) REFERENCES Sugar(idSugar),
FOREIGN KEY (idDrink) REFERENCES Drink(idDrink),
FOREIGN KEY (idDixieCup) REFERENCES DixieCup(idDixieCup) 
);

INSERT INTO Customer VALUES 
(1, 'Bizot--Pageot', 'Kyllian', 'Kyllian', 'admin', 'mathsmathsmaths'), 
(2, 'Camembert', 'Bastien', 'Camemba', 'admin', 'shibainu'),
(3, 'Ciron', 'Thomas', 'Gueriyox', 'admin', 'thomaslechef'),
(4, 'Dargent', 'Gaëtan', 'Xdargent', 'admin', '123456'),
(5, 'De Gryse', 'Antoine', 'Pitbull', 'admin', 'cestquoimonmdpdeja'),
(6, 'Perritaz', 'Julien', 'yveneas', 'admin', 'password'),
(7, 'Boon', 'Dany', 'Dany Boon', 'user', 'chtipower');

INSERT INTO Sugar VALUES (1, 28);

INSERT INTO Drink VALUES
(1, "Nespresso", 2.50, 500),
(2, "Nestle", 3.14, 500),
(3, "Nescafe", 2.7, 500),
(4, "Grand-mere", 0.99, 20); 

INSERT INTO DixieCup VALUES
(1, 10, 500),
(2, 16, 400),
(3, 18, 350),
(4, 20, 325),
(5, 23, 310),
(6, 24, 5);

INSERT INTO Command VALUES
(1, 0, 1, NULL, 1, 1),
(2, 2, 5, 1, 2, 4),
(3, 3, 7, 1, 3, 2),
(4, 4, 6, 1, 3, 6),
(5, 0, 2, NULL, 3, 1),
(6, 1, 4, 1, 1, 1),
(7, 5, 4, 1, 4, 5),
(8, 4, 1, 1, 1, 3),
(9, 5, 4, 1, 1, 1),
(10, 4, 7, 1, 2, 4),
(11, 2, 5, 1, 3, 1),
(12, 4, 7, 1, 4, 5),
(13, 0, 3, NULL, 4, 3),
(14, 5, 7, 1, 3, 3),
(15, 3, 7, 1, 3, 5),
(16, 4, 7, 1, 1, 6),
(17, 4, 6, 1, 3, 4),
(18, 0, 7, NULL, 2, 5),
(19, 2, 1, 1, 2, 2),
(20, 1, 2, 1, 1, 6),
(21, 4, 3, 1, 1, 4),
(22, 1, 2, 1, 4, 2),
(23, 2, 2, 1, 4, 4),
(24, 0, 3, NULL, 2, 1),
(25, 1, 3, 1, 4, 4),
(26, 3, 3, 1, 4, 4),
(27, 2, 6, 1, 4, 4),
(28, 1, 1, 1, 2, 5),
(29, 0, 2, NULL, 4, 6);